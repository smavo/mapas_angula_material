## MAPA [ANGULAR MATERIAL, GOOGLE MAPS]
Aplicacion de peticiones

## Install
npm install

## Use in development
ng serve

## Preview
![image](/uploads/d1725c39ebed30b63864dd4d57ef5e3c/image.png)
![image](/uploads/d2b8576efc26dd1717a3d9957020ea02/image.png)
![image](/uploads/b5395c441202e6cabfa95de31d19ceb3/image.png)
![image](/uploads/154133491f06ec751084464f9a7a8ed6/image.png)


## Creator of the MAPA [ANGULAR MATERIAL, GOOGLE MAPS] project
:bust_in_silhouette: **Sergio V.O**
* Twitter: [@https://twitter.com/smavo24](https://twitter.com/smavo24)
* Github: [@smavo](https://github.com/smavo)
* Gitlab: \[@smavo\] (https://gitlab.com/smavo)
* LinkedIn: [@https://www.linkedin.com/in/smavo24/](https://www.linkedin.com/in/smavo24/)



# Mapas

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
