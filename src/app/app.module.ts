import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Modulo creado de Angular Material
import {MaterialModule} from './material.module';
import { MapaComponent } from './components/mapa/mapa.component'

import { GoogleMapsModule } from '@angular/google-maps'
import { AgmCoreModule } from '@agm/core';
import { MapaEditarComponent } from './components/mapa/mapa-editar.component'
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  entryComponents:[
    MapaEditarComponent
  ],
  declarations: [
    AppComponent,
    MapaComponent,
    MapaEditarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    GoogleMapsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBG9HMhkqAHBIYFQrIGB1blKXSyzgPmz5Q'
      /*apiKey: 'AIzaSyAZK4GGsOj4l4k2A1XBSZZTeO9kI8ljy9w'*/
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
