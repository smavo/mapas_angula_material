import { Component, OnInit } from '@angular/core';
import { Marcador } from '../../classes/marcador.class';
import { MatSnackBar } from "@angular/material/snack-bar";
import {MatDialog,MatDialogRef} from '@angular/material/dialog';
import { MapaEditarComponent } from './mapa-editar.component';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  marcadores: Marcador[] = [];

  lat = -12.13220009918839;
  lng = -77.03047617898335;

  constructor(private _snackBar: MatSnackBar, public dialog: MatDialog) {
    //const nuevoMarcador = new Marcador(-12.13220009918839, -77.03047617898335);
    //this.marcadores.push( nuevoMarcador);
    if(localStorage.getItem('marcadores')){
      this.marcadores = JSON.parse(localStorage.getItem('marcadores'));
    }
  }

  ngOnInit(): void {}

  agregarMarcador( evento ){
    const coords: { lat: number; lng: number } = evento.coords;
    console.log(evento.coords);
    const nuevoMarcador = new Marcador(coords.lat, coords.lng);
    this.marcadores.push( nuevoMarcador);
    this.guardarStorage();
    this._snackBar.open("Marcador registrado", "Cerrar", { duration: 3000 });
  }


  guardarStorage() {
    localStorage.setItem("marcadores", JSON.stringify(this.marcadores));
  }


  borrarMarcador(i: number) {
    this.marcadores.splice(i, 1); //borrar un solo elemento
    this.guardarStorage();
    this._snackBar.open("Marcador borrado", "Cerrar", { duration: 3000 });
  }


  editarMarcador(marcador:Marcador){
    const dialogRef = this.dialog.open( MapaEditarComponent, {
      width: '250px',
      data: {titulo: marcador.titulo, desc: marcador.desc}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(!result){
        return;
      }
      marcador.titulo = result.titulo;
      marcador.desc = result.desc;
   
    this.guardarStorage();
    this._snackBar.open("Marcador actualizado", "Cerrar", { duration: 3000 });
  });
  
  }

}


